package nl.bioinf.rtseq.control;

import nl.bioinf.rtseq.model.*;
import nl.bioinf.rtseq.nl.bioinf.rtseq.sequencer.Sequencer;

public class Main {
    public static Sequencer sqr;
    public static void main(String[] args) {
        //create sequencer and dummy sequences
        sqr = new Sequencer();
        sqr.createDummySequences(25, 25);

        //create sequence quality observer and its filter and register it
        SequenceQualityObserver qualObs = new SequenceQualityObserver();
        qualObs.addSequenceFilter(new SequenceQualityFilter());
        sqr.registerObserver(qualObs);

        //create pathogenicity observer and its filter and register it
        SequencePathogenicityObserver pathObs = new SequencePathogenicityObserver();
        pathObs.addSequenceFilter(new EnterococcusPathogenFilter());
        pathObs.addSequenceFilter(new StaphAureusPathogenFilter());
        sqr.registerObserver(pathObs);

        //start sequencing
        sqr.sequence();



    }
}
