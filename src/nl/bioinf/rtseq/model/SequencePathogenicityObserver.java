package nl.bioinf.rtseq.model;

import nl.bioinf.rtseq.nl.bioinf.rtseq.sequencer.SequencerObserver;

/**
 * Created by michiel on 07/03/2017.
 */
public class SequencePathogenicityObserver extends AbstractSequenceObserver implements SequencerObserver{
    @Override
    public void newSequenceDetermined(Sequence sequence) {
        //System.out.println(this.getFilter());
        boolean accept = this.applyFilters(sequence);
        if (!accept) {
            System.out.println(">>>" + this.getClass().getSimpleName() + " has found a pathogenic bacterium; alerting CDC!");
            //alert CDC
        }
    }
}
