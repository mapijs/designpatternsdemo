package nl.bioinf.rtseq.model;

import nl.bioinf.rtseq.nl.bioinf.rtseq.sequencer.SequencerObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michiel on 07/03/2017.
 */
public abstract class AbstractSequenceObserver implements SequencerObserver  {
    private List<SequenceFilter> filters;

    public void addSequenceFilter(SequenceFilter filter) {

        if (null == this.filters){
            this.filters = new ArrayList <>();
        }
        this.filters.add(filter);
    }

    public List<SequenceFilter> getFilters() {
        return filters;
    }

    public void clearFilters() {
        this.filters.clear();
    }


    public boolean applyFilters(Sequence sequence) {
        for(SequenceFilter sf : this.filters) {
            if (!sf.filter(sequence)) {
                return false;
            }
        }
        return true;
    }
}
