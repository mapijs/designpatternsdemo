package nl.bioinf.rtseq.model;

/**
 * Created by michiel on 07/03/2017.
 */
public interface SequenceFilter {
    /**
     * method accepts a sequence object and tells whether
     * it passes the implementers' criteria
     * @param sequence
     * @return
     */
    public boolean filter(Sequence sequence);

}
