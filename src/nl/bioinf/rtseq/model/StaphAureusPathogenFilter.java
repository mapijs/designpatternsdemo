package nl.bioinf.rtseq.model;

/**
 * Created by michiel on 14/03/2017.
 */
public class StaphAureusPathogenFilter implements SequenceFilter{
    @Override
    public boolean filter(Sequence sequence) {
        return ! sequence.getSequence().contains("TTT");
    }
}
