# Design Patterns Demo

## Purpose
This repo is for demonstration purposes only.
It has been created for the course Application Design of the curriculum Bioinformatics.
It contains some embedded design patterns in a slightly mixed manner. Can you find them all?
